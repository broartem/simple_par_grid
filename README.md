# Simple parallel grid reader / writer

## Installation

From git:

	cd /working/directory/path
	git clone https://github.com/broartem/simple_par_grid.git

## Running

By MPI:

    CXX="mpicxx" CC="mpicc" cmake ./
    make
    ./simple_par_grid