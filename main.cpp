#include "main.h"

int main(int argc, char const *argv[])
{
	LPLPLPFLOAT test;
	char target[50];
	char grid_name[50];
	char func_name[50];
	big_size file_x;
	big_size file_y;
	big_size file_z;

	test = alloc_float_mas_n1_n2_n3 (10, 10, 10);

	if(!test) {
		printf("Array allocation error\n");
		return 1;
	}

	for (int i = 0; i < 10; ++i)
	{
		for (int j = 0; j < 10; ++j)
		{
			for (int k = 0; k < 10; ++k)
			{
				test[i][j][k] = (float)(i + 10*j + 100*k);
			}
		}
	}

	sprintf (target, "data");
	sprintf (grid_name, "grid");

	file_x = 10;
	file_y = 10;
	file_z = 10;

	int status = WriteBjnGzippedScalar8RecInit(target, grid_name,
		file_x, file_y, file_z);

	if (status > 0) {
		printf("File initialization error\n");
		return 1;
	}

	sprintf (func_name, "test_func");
	int minValue = (float)0;
	int maxValue = (float)10;

	status = WriteBjnGzippedScalar8RecFuncMinMax(target, func_name,
		minValue, maxValue);

	if (status > 0) {
		printf("File write min/max value error\n");
		return 1;
	}

	status = WriteBjnGzippedScalar8RecFunc(target, func_name,
		test, file_x, file_y, file_z, 1);

	if (status > 0) {
		printf("File write function values error\n");
		return 1;
	}

	LPLPLPFLOAT readed;

	readed = alloc_float_mas_n1_n2_n3 (20, 20, 20);

	if(!readed) {
		printf("Array allocation error\n");
		return 1;
	}

	int x;
	int y;
	int z;

	status = ReadBjnGzippedScalar8RecFunc(target, func_name,
		&readed, &x, &y, &z);

	if (status > 0) {
		printf("File initialization error\n");
		return 1;
	}

	printf("x: %d, y: %d, z: %d\n", x, y, z);

	for (int i = 0; i < x; ++i)
	{
		printf("layer i = %d:\n", i);

		for (int j = 0; j < y; ++j)
		{
			for (int k = 0; k < z; ++k)
			{
				printf("%f ", readed[i][j][k]);
			}

			printf("\n");
		}

		printf("\n\n");
	}

	return 0;
}